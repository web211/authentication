package dhbw.web.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityTokenConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private JwtConfig jwtConfig;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		return;
		/*
		 * http.csrf().disable().sessionManagement().sessionCreationPolicy(
		 * SessionCreationPolicy.STATELESS).and() .exceptionHandling()
		 * .authenticationEntryPoint((req, rsp, e) ->
		 * rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED)).and() .addFilter(new
		 * JWtTokenAuthenticationFilter(jwtConfig),
		 * UsernamePasswordAuthenticationFilter.class)
		 * .authorizeRequests().antMatchers(HttpMethod.POST,
		 * jwtConfig.getUri().permitAll()).anyRequest() .authenticate();
		 */
	}

	@Bean
	public JwtConfig jwtConfig() {
		return new JwtConfig();
	}

}
