package dhbw.web.auth;

import org.springframework.beans.factory.annotation.Value;

import lombok.Getter;

public class JwtConfig {


    @Value("${security.jwt.uri:/auth/**}")
    @Getter
    private String Uri;

    @Value("${security.jwt.header:Authorization}")
    @Getter
    private String header;

    @Value("${security.jwt.prefix:Bearer }")
    @Getter
    private String prefix;

    @Value("${security.jwt.expiration:#{24*60*60}}")
    @Getter
    private int expiration;

    @Value("${security.jwt.secret:JwtSecretKey}")
    @Getter
    private String secret;
}
