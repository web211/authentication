
package dhbw.web.auth;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.service.AuthenticationService;
import dhbw.web.model.User;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/")
@Slf4j
public class AuthController implements AuthenticationService {

	@LoadBalanced
	@GetMapping(path = "login")
	public User login(@RequestBody User user) {
		log.warn("Read: received the object: " + user);
		return user;
	}

	@LoadBalanced
	@GetMapping(path = "logout")
	public void logout() {
		log.warn("Read: received the object: ");
	}

	public Boolean isLoggedin() {
		return true;
	}

	// https://jwt.io/introduction/
	// https://dzone.com/articles/json-web-tokens-with-spring-cloud-microservices

	@Override
	public void save(User user) {
		// TODO Auto-generated method stub

	}

	@Override
	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		return null;
	}

}
